// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/02/FullAdder.hdl

/**
 * Computes the sum of three bits.
 * sum = na*nb*c + na*b*nc + a*nb*nc + abc
 *     = ^a(^bc + b^c) + a(^b^c + bc)
 *     =    v1     v2       v5     v6
 *             v3              v7
 *          v4              v8
  */

CHIP FullAdder {
    IN a, b, c;  // 1-bit inputs
    OUT sum,     // Right bit of a + b + c
        carry;   // Left bit of a + b + c

    PARTS:
    Not(in=a, out=na);
    Not(in=b, out=nb);
    Not(in=c, out=nc);
    And(a=nb, b=c, out=v1);
    And(a=b, b=nc, out=v2);
    Or(a = v1, b=v2, out=v3);
    And(a=na, b=v3, out=v4);
    And(a=nb, b=nc, out=v5);
    And(a=b, b=c, out=v6);
    Or(a=v5, b=v6, out=v7);
    And(a=a, b=v7, out=v8);
    Or(a=v4, b=v8, out=sum);
/*
 * car = ^abc + a^bc +ab^c + abc
 *     = bc(^a+a) + a^bc + ab^c
 *     = bc + a(^bc + b^c)
 *     = w1      w2    w3
 *                  w4
 *              w5
 */
    And(a=b, b=c, out=w1);
    And(a=nb, b=c, out=w2);
    And(a=b, b=nc, out=w3);
    Or(a=w2, b=w3, out=w4);
    And(a=a, b=w4, out=w5);
    Or(a=w1, b=w5, out=carry);
}
