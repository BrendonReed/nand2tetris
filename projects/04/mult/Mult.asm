// Multiplies R0 and R1 and stores the result in R2.
// (R0, R1, R2 refer to RAM[0], RAM[1], and RAM[2], respectively.)
// R0 and R1 are >= 0
// add r0 to 0 r2 times
// add r0 to r1 store in r2
    @R2
    M=0
    @R1
    D=M
    @END
    D;JEQ
    @R0
    D=M
    @R2 
    M=D
    @i //17
    M=1
    
(LOOP)
    @R1
    D=M
    @i
    D=D-M
    @END
    D;JLE //if i >= r1 then quit

    //otherwise, 
    //increment i
    @i
    D=M+1
    M=D
    //add r0 to running and loop
    @R0
    D=M
    @R2
    D=D+M
    M=D
    @LOOP
    0;JMP
(END)
    @END
    0;JMP

