// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Fill.asm

// Runs an infinite loop that listens to the keyboard input. 
// When a key is pressed (any key), the program blackens the screen,
// i.e. writes "black" in every pixel. When no key is pressed, the
// program clears the screen, i.e. writes "white" in every pixel.

// Put your code here.
//16384 (0x4000) is start of screen memory map
//it's 256 row x 512 columns
//each row is 32 consecutive 16 bit words
//16 bits is 65536, but it's signed, so 32,767, so that's not quite right, 
//so -1 is all 1's

(RESTART)
    @i
    M=0
    @SCREEN
    D=A
    @current
    M=D

(LOOP)
    @24576 //keyboard
    D=M
    @BLACK
    D;JGT
    //WHITE
    @0
    D=A
    @FILL
    0;JMP
(BLACK)
    D=-1
(FILL)
    @current
    A=M
    M=D
    //add 1 to @current and loop
    @current
    D=M+1
    M=D
    @i
    D=M+1
    M=D
    @8192
    D=A-D
    @RESTART
    D;JLE
    @LOOP
    0;JMP

