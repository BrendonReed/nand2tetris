// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/01/DMux4Way.hdl

/**
 * 4-way demultiplexor:
 * {a, b, c, d} = {in, 0, 0, 0} if sel == 00
 *                {0, in, 0, 0} if sel == 01
 *                {0, 0, in, 0} if sel == 10
 *                {0, 0, 0, in} if sel == 11
 * i s s | a b c d
 * 0 0 0 | 0 0 0 0
 * 0 0 1 | 0 0 0 0
 * 0 1 0 | 0 0 0 0
 * 0 1 1 | 0 0 0 0
 * 1 0 0 | 1 0 0 0
 * 1 0 1 | 0 1 0 0
 * 1 1 0 | 0 0 1 0
 * 1 1 1 | 0 0 0 1
*
 * a = i ns0 ns1
 * b = i s0 ns1
 * c = i s1 ns0
 * d = i s0 s1
 */

CHIP DMux4Way {
    IN in, sel[2];
    OUT a, b, c, d;

    PARTS:
    Not(in=sel[0], out=ns0);
    Not(in=sel[1], out=ns1);
    And3Way(a=in, b=ns0, c=ns1, out=a);
    And3Way(a=in, b=ns1, c=sel[0], out=b);
    And3Way(a=in, b=sel[1], c=ns0, out=c);
    And3Way(a=in, b=sel[0], c=sel[1], out=d);
}
