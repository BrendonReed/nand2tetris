module JackParser
open System
open System.IO

type Result<'TSuccess,'TFailure> = 
    | Success of 'TSuccess
    | Failure of 'TFailure

type lexical = 
    | IntegerConstant of UInt16
    | StringConstant of string
    | IdentToken of string
    | Keyword of string
    | Symbol of char
    | Comment of string
//lex stream of characters into digit tokens
let toList s = (s :> char seq) |> Seq.toList

let toString (s: char list) = 
    let stringed = List.map (fun f -> f.ToString()) s
    List.fold (+) "" stringed

let toInt (s:char list) = 
    UInt16.Parse((toString s))

let keywords = [|
    "class"; "constructor"; "function"; "method"; "field";
    "static"; "var"; "int"; "char"; "boolean"; "void"; "true";
    "false"; "null"; "this"; "let"; "do"; "if"; "else";
    "while"; "return" |]

let isKeyword (s: char list) = Array.contains (toString s) keywords
let symbols = "(){}[].,;+-*/&|<>=~".ToCharArray()
let stopcases = Array.concat [symbols; ("\n\t ".ToCharArray())]
let stopcase c = Array.contains c stopcases

let rec lexDigit soFar rest =
    match rest with 
    | [] -> (IntegerConstant (toInt soFar), [])
    | x::xs -> 
        if stopcase x then (IntegerConstant (toInt soFar), x::xs)
        else if Char.IsDigit(x) then lexDigit (soFar @ [x]) xs 
        else failwith "no"

let rec lexStringConstant soFar rest =
    match rest with
    | [] -> (StringConstant (toString soFar), [])
    | x::xs ->
        if x = '"' then (StringConstant (toString soFar), xs)
        else if x = '\n' then failwith "unexpected newline in string constant"
        else lexStringConstant (soFar @ [x]) xs

let rec lexKeywordOrIdentifier soFar rest =
    let terminate value = 
        if (isKeyword value) then (Keyword (toString soFar), rest)
        else (IdentToken (toString value), rest)
    match rest with
    | [] -> terminate soFar
    | x::xs ->
        if stopcase x then terminate soFar
        else lexKeywordOrIdentifier (soFar @ [x]) xs

let rec lineComment (soFar: char list) rest =
    match rest with 
    | [] -> failwith "line comment with no eol"
    | x::xs -> 
        match x with 
        | '\n' -> Comment (toString soFar), xs
        | _ -> lineComment (soFar @ [x]) xs

let rec isCommentEnd soFar rest =
    match rest with
    | [] -> failwith "comment block started but not ended!"
    | x::xs ->
        match x with
        | '/' -> Comment (toString (soFar)), xs
        | _ -> startComment soFar rest
and startComment (soFar: char list) (rest: char list) =
    match rest with 
    | [] -> ((Comment (toString soFar)), rest)
    | x::xs -> 
        match x with
        | '*' -> isCommentEnd (soFar @ [x]) xs
        | '/' -> lineComment soFar xs
        | _ -> startComment (soFar @ [x]) xs

let rec lex (s: char list) : lexical list =
    match s with
    | [] -> []
    | '\n'::xs -> lex xs
    | '\r'::xs -> lex xs
    | ' '::xs -> lex xs
    | '\t'::xs -> lex xs
    | '/'::xs -> 
        let (soFar, rest) = startComment [] xs
        soFar :: lex rest
    | x::xs when Array.contains x symbols -> (Symbol x) :: lex xs
    | x::xs -> 
        let (soFar, rest) = 
            match x with
            | '/' -> startComment [] xs
            | '"' -> lexStringConstant [] xs
            | _ when Char.IsDigit(x) -> lexDigit [x] xs
            | _ -> lexKeywordOrIdentifier [x] xs
        soFar :: lex rest

let toXml (lexeme: lexical) = 
    let r = 
        match lexeme with
        | IntegerConstant v -> "<integerConstant> " + v.ToString() + " </integerConstant>\n"
        | StringConstant v -> "<stringConstant> " + v.ToString() + " </stringConstant>\n"
        | IdentToken v -> "<identifier> " + v.ToString() + " </identifier>\n"
        | Keyword v -> "<keyword> " + v.ToString() + " </keyword>\n"
        | Symbol v -> 
            let s = 
                match v with
                | '<' -> "&lt;"
                | '&' -> "&amp;"
                | '>' -> "&gt;"
                | _ -> v.ToString()
            "<symbol> " + s + " </symbol>\n"
        | Comment v -> ""
    r

let toXmlStrings tokens = 
    tokens 
    |> List.map toXml
    |> List.reduce (+)

let lexFileToXml path =
    //open Square.jack, read it into one big string
    let fileText = File.ReadAllText(path)
    let tokens = lex (toList (fileText))
    let tokenXmlStrings = toXmlStrings tokens
    let xml = "<tokens>\n" + tokenXmlStrings + "</tokens>"
    xml

let infile = @"C:\Users\breed\Development\nand2tetris\projects\10\Square/Square.jack"
let outpath = @"C:\temp\SquareT.xml"
//let xml = lexFileToXml infile
//File.WriteAllText(outpath, xml)

type Identifier = Identifier of string
type Type = IntS | CharS | BoolS | ClassName of string
type VarName = VarName of Type * Identifier
type ParamList = ParamList of VarName list
type ClassVarDec = 
    | Static of VarName list
    | ClassField of VarName list
type VarDec = VarDec of VarName list
type KeywordConstant = TrueC|FalseC|NullC|ThisC
type Statement =
    | LetStatement
    | IfStatement
    | WhileStatement
    | DoStatement
    | ReturnStatement
and SubroutineType = Type of Type | Void
and SubroutineBody = SubroutineBody of VarDec * Statement list
and SubroutineName = SubroutineName of Identifier
and SubroutineParts = { 
    SubroutineType:SubroutineType
    Name:SubroutineName
    ParamList:ParamList
    SubroutineBody:SubroutineBody
    }
and SubroutineDec = 
    | Ctor of SubroutineParts
    | Function of SubroutineParts
    | Method of SubroutineParts

and UnaryOp = Negate | Tilde
and Op = Plus | Minus | Mult | Div | Amp | Pipe | Lt | Gt | Eq
and Expression = ExpressionT of Term * OpTerm list
and OpTerm = OpTerm of Op * Term
and SubroutineCall = 
    | NakedCall of SubroutineName * ExpressionList
    | ClassCall of Identifier * SubroutineName * ExpressionList
and Term = 
    | IntegerConstantT of UInt16
    | StringConstantT of string
    | KeywordConstantT of KeywordConstant
    | Var of string
    | VarWithIndex of Identifier * Expression
    | SubroutineCallE of SubroutineCall
    | Expression of Expression
    | UnaryOpTerm of UnaryOp * Term
and ExpressionList = ExpressionList of Expression list
and ReturnStatement = ReturnStatement of Expression option
and DoStatement = DoStatement of SubroutineCall
and WhileStatement = WhileStatement of Expression * Statement list
and IfStatement = IfStatement of Expression * Statement list * Statement list
and LetStatement = LetStatement of VarName * Expression option * Expression

let parseKeywordConstant rest : KeywordConstant * lexical list =
    match rest with 
    | [] -> failwith "expected keyword, found empty"
    | x::xs ->
        match x with 
        | Keyword "true" -> (TrueC, xs)
        | Keyword "false" -> (FalseC, xs)
        | Keyword "null" -> (NullC, xs)
        | Keyword "this" -> (ThisC, xs)
        | _ -> failwith (sprintf "expected keyword, found %A" x)

let parseOp rest : (Op * lexical list) option =
    match rest with 
    | [] -> None
    | x::xs ->
        match x with 
        | Symbol '+' -> Some (Plus, xs)
        | Symbol '-' -> Some (Minus, xs)
        | Symbol '*' -> Some (Mult, xs)
        | Symbol '/' -> Some (Div, xs)
        | Symbol '&' -> Some (Amp, xs)
        | Symbol '|' -> Some (Pipe, xs)
        | Symbol '<' -> Some (Lt, xs)
        | Symbol '>' -> Some (Gt, xs)
        | Symbol '=' -> Some (Eq, xs)
        | _ -> None

let rec parseTerm rest : Term * lexical list =
    match rest with
    | [] -> failwith "expected term, found empty"
    | x::xs ->
        match x with
        | IntegerConstant v -> (IntegerConstantT v, xs)
        | StringConstant v -> (StringConstantT v, xs)
        | Keyword v -> 
            let kw, after = parseKeywordConstant rest
            (KeywordConstantT kw, after)
        | Symbol '-' -> 
            let thisTerm, afterTerm = parseTerm xs
            (UnaryOpTerm (Negate, thisTerm)), afterTerm
         | Symbol '~' -> 
            let thisTerm, afterTerm = parseTerm xs
            (UnaryOpTerm (Tilde, thisTerm)), afterTerm
        | IdentToken v -> 
            //Var v, xs
            // | varName '[' expression ']'
            // | subroutineName(paramsList)
            match xs with
            | [] -> Var v, xs
            | h::t ->
                match h with
                | Symbol '[' -> 
                    let exp, r2 = parseExpression t
                    match r2 with
                    | (Symbol ']')::more -> 
                        let x = (VarWithIndex ((Identifier v), exp))
                        x, more
                    | _ -> failwith "expected ]"

                | Symbol '(' -> 
                    //parse subroutine call
                    // | IdentToken '(' expressionList ')'
                    // | (className | varName) '.' IdentToken '(' expressionList ')'
                    let thisTerm, afterTerm = parseSubroutineCall rest
                    (SubroutineCallE thisTerm), afterTerm
                | _ -> Var v, xs
        | Symbol '(' -> 
            let expr, afterExpression = parseExpression xs
            match afterExpression with
            | [] -> failwith "expected matching ) after expression"
            | h::t -> 
                match h with 
                | Symbol ')' -> ((Expression expr), t)
                | _ -> failwith "expected matching ) after expression"
        | _ -> failwith (sprintf "expected term, found %A" x)
and parseExpressionH (firstTerm: Term option) (opTerms:OpTerm list) (rest:lexical list) : Expression * lexical list =
    match firstTerm with
    | None -> 
        let thisTerm, afterTerm = parseTerm rest
        parseExpressionH (Some thisTerm) [] afterTerm
    | Some value -> 
        let maybeOp = parseOp rest
        match maybeOp with
        | Some parsed ->
            let o, afterOp = parsed
            let nextTerm, afterTerm = parseTerm afterOp
            parseExpressionH (Some value) (opTerms @ [OpTerm (o, nextTerm)]) afterTerm
        | None -> (ExpressionT (value, opTerms)), rest
and parseExpression rest : Expression * lexical list =
    parseExpressionH None [] rest
and parseSubroutineCall (rest:lexical list) : SubroutineCall * lexical list =
    //SubroutineName '(' expressionList ')' 
    // | (classname|varname) '.' SubroutineName '(' expresssionList ')'
    match rest with
    | [] -> failwith "expected subroutine call, found nothing"
    | (IdentToken x)::xs -> //x is either Subroutine name, or class|varname, check xs.first to see which
        match xs with
        | [] -> failwith "wrong"
        | h::t -> 
            match h with
            | (Symbol '(') -> 
                let arguments, after = parseExpressionList [] t
                //after should start with ')'
                match after with
                | (Symbol ')'):: afterArgs -> 
                    (NakedCall (SubroutineName (Identifier x), arguments)), afterArgs
                | _ -> failwith "expected ')' after arguments"
            | (Symbol '.') -> 
                match t with
                | [] -> failwith "wrong"
                | (IdentToken subName) :: after3 -> 
                    let arguments, after = parseExpressionList [] after3
                    match after with
                    | (Symbol ')'):: afterArgs -> 
                        (ClassCall (Identifier x, SubroutineName (Identifier subName), arguments)), afterArgs
                    | _ -> failwith "expected ')' after arguments"
                | _ -> failwith "expected subroutine name, found "
            | _ -> failwith "expected ( or . after member, class or sub name"
    | _ -> failwith (sprintf "expected member, class or sub name got %A" rest)
and parseExpressionList (prevExpressions:Expression list) (rest: lexical list) : ExpressionList * lexical list =
    match rest with
    | [] -> failwith "expected ')'"
    | x::xs -> 
        match x with
        | Symbol ')' -> ExpressionList [], rest
        | _ ->
            let expr, after = parseExpression rest
            match after with
            | [] -> ExpressionList (prevExpressions @ [expr]), after
            | h::t ->
                match h with
                | (Symbol ',') -> parseExpressionList (prevExpressions @ [expr]) after
                | _ -> ExpressionList (prevExpressions @ [expr]), after

//read type from rest, advance past type, output type
let rec parseType (rest) =
    match rest with
    | [] -> failwith "bad type def"
    | s::xs ->
        let r = 
            match s with
            | (Keyword v) -> 
                match v with
                | "int" -> IntS
                | "char" -> CharS
                | "boolean" -> BoolS
                | _ -> failwith (sprintf "unexpected %A, expecting int|char|boolean" v)
            | (IdentToken v) -> ClassName v
            | _ -> failwith "expected keyword or identifier"
        (r, xs)

let parseOneVarDec (rest) =
    //type identifier
    let (myType, afterType) = parseType rest
    match afterType with 
    | [] -> failwith "expected variable name"
    | h::t -> 
        match h with 
        | (IdentToken v) -> (VarName (myType, (Identifier v)), t)
        | _ -> failwith "expected identifier"

let rec parseVarName (soFar) (rest) =
    //if , go again, if ; bail
    let takeOne, after = parseOneVarDec rest
    match after with
    | [] -> failwith "expected , or ; after vardec"
    | x::xs when x = (Symbol ';') -> [takeOne] @ soFar, xs
    | x::xs when x = (Symbol ',') -> (parseVarName ([takeOne] @ soFar) xs)
    | _ -> failwith "expected , or ; after vardec"

let rec parseParamList (soFar) (rest) =
    match rest with
    | [] -> soFar, []
    | (Symbol ')')::xs -> 
        soFar, xs
    | (Symbol ',')::xs ->
        parseParamList soFar xs
    | _ -> 
        let takeOne, after = parseOneVarDec rest
        parseParamList (soFar @ [takeOne]) after

let parseVarDec (soFar) (rest) =
    match rest with
    | [] -> (VarDec [], rest)
    | x::xs -> 
        match x with
        | (Keyword "var") -> 
            let a,b = parseVarName [] xs
            (VarDec a), b
        | _ -> (VarDec [], rest)

let rec parseSubroutineStatements soFar rest =
    match rest with
    | (Symbol '}') :: xs -> soFar, []
    | _ -> failwith "subroutine statements not implemented yet"

let rec parseSubroutineBody soFar rest =
    match rest with
    | [] -> failwith "missing subroutine body"
    | x::xs ->
        match x with
        | (Symbol '{') -> 
            let varDecs,afterVarDecs = parseVarDec [] xs
            let statements, afterStatements = parseSubroutineStatements [] afterVarDecs 
            (SubroutineBody (varDecs, statements)), afterStatements
        | _ -> failwith "expected {"

let parseSubroutineDec soFar rest =
    let parseFunction t xs = 
        match xs with
        | (IdentToken subName) :: (Symbol '(') :: afterName -> 
            let paramList, afterParams = parseParamList [] afterName
            let body, afterBody = parseSubroutineBody [] afterParams
            let parts = { 
                SubroutineType=t; 
                Name=(SubroutineName (Identifier subName)); 
                ParamList=(ParamList paramList); 
                SubroutineBody=body
                }
            parts, afterBody
        | _ -> failwith "expected subroutine name"
    let parseDecType xs =
        match xs with
        | (Keyword "void")::afterType -> 
            parseFunction Void afterType
        | _ ->
            let returnType, afterType = parseType xs
            parseFunction (Type returnType) afterType

    match rest with
    | (Keyword "constructor") :: xs -> 
        let parts, afterBody = parseDecType xs
        (Ctor parts), afterBody
    | (Keyword "function") :: xs -> 
        let parts, afterBody = parseDecType xs
        (Function parts), afterBody
    | (Keyword "method") :: xs -> 
        let parts, afterBody = parseDecType xs
        (Method parts), afterBody
    | _ -> failwith "expected subroutine declaration"

let rec parseClassVarDec (rest) =
    //static|field varDec (, vardec)* ;
    match rest with
    | [] -> failwith "bad classvardec"
    | x::xs -> 
        match x with 
        | (Keyword "static") -> 
            let (vd, afterVarDec) = (parseVarName [] xs)
            (Static vd, afterVarDec)
        | (Keyword "field") -> 
            let (vd, afterVarDec) = (parseVarName [] xs)
            (ClassField vd, afterVarDec)
        | _ -> failwith "expected static|field"

