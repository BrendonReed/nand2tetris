#load "jackParser.fsx"

open JackParser
let mutable section = ""
let mutable status  = 0
let mutable count   = 0

exception Fail of string

let runATest (name: string) test =
    try
        test ()
        count <- count + 1
    with
    | Fail reason ->
        status <- 1
        stderr.WriteLine("FAIL: {0}.{1} -- {2}", section, name, reason)
    | e ->
        status <- 1
        stderr.WriteLine("FAIL: {0}.{1}", section, name)
        stderr.WriteLine(e)

type Test(name: string) =
    member this.Delay(f : unit -> unit) = runATest name f
    member this.Zero() = ()

let Section name =
    section <- name

let Throws<'T when 'T :> exn> f =
    try
        f ()
        raise (Fail (sprintf "Does not throw: %O" typeof<'T>))
    with
    | :? 'T ->
        ()

let ( =? ) a b =
    if a <> b then
        raise (Fail (sprintf "Expected %A and got %A." a b))

let ( <>? ) a b =
    if a = b then
        raise (Fail (sprintf "Unexpected %A." a))

let runTests () =
    Section "Arithmetic"

    Test "addition" {
        1 + 1 =? 2
        //(1 + 0) =? 0
    }
    Test "structure" {
        parseType [(Keyword "int")] =? (IntS, [])
        //parseType [(Keyword "fke")] =? (IntS, [])
        parseType [(Keyword "char")] =? (CharS, [])
        parseType [(IdentToken "Complex")] =? (ClassName "Complex", [])

        parseVarDec [] [
            (Keyword "var"); (Keyword "int"); (IdentToken "x"); (Symbol ',')
            (Keyword "int"); (IdentToken "y"); (Symbol ',');
            (Keyword "boolean"); (IdentToken "ab"); (Symbol ';');
            ]
            =? (VarDec
             [VarName (BoolS,Identifier "ab"); VarName (IntS,Identifier "y");
              VarName (IntS,Identifier "x")], [])
        parseVarDec [] [
            (Keyword "var"); (Keyword "int"); (IdentToken "x"); (Symbol ';')
            ]
            =? (VarDec [VarName (IntS,Identifier "x")], []) 
        
        // type varname,
        parseParamList [] []
            =? ([], [])
        parseParamList [] [(Keyword "int"); (IdentToken "x")] 
            =? ([VarName (IntS,Identifier "x")], [])
        parseParamList [] [
            (Keyword "int"); (IdentToken "x"); (Symbol ','); 
            (Keyword "int"); (IdentToken "y");
            (Symbol ')');
            ]
            =? ([VarName (IntS,Identifier "x"); VarName (IntS,Identifier "y")], [])

        parseSubroutineBody [] [(Symbol '{'); (Symbol '}');]
            =? (SubroutineBody (VarDec [], []), [])
        parseSubroutineBody [] [
            (Symbol '{'); 
            (Keyword "var"); (Keyword "int"); (IdentToken "x"); (Symbol ',')
            (Keyword "int"); (IdentToken "y"); (Symbol ',');
            (Keyword "boolean"); (IdentToken "ab"); (Symbol ';');
            (Symbol '}');
            ] 
            =? (SubroutineBody
                 (VarDec
                    [VarName (BoolS,Identifier "ab"); VarName (IntS,Identifier "y");
                     VarName (IntS,Identifier "x")],[]), [])

        parseSubroutineDec [] [
            (Keyword "constructor"); (Keyword "void"); (IdentToken "noop");
            (Symbol '(');
            (Symbol ')');
            (Symbol '{'); 
            (Symbol '}');
            ]
            =? (Ctor {SubroutineType = Void;
                 Name = (SubroutineName (Identifier "noop"));
                 ParamList = ParamList [];
                 SubroutineBody = SubroutineBody (VarDec [],[]);}, [])

        parseSubroutineDec [] [
            (Keyword "constructor"); (Keyword "void"); (IdentToken "noop");
            (Symbol '(');
            (Keyword "int"); (IdentToken "x");
            (Symbol ')');
            (Symbol '{'); 
            (Symbol '}');
            ]
            =? (Ctor {SubroutineType = Void;
                 Name = (SubroutineName (Identifier "noop"));
                 ParamList = ParamList [VarName (IntS,Identifier "x")];
                 SubroutineBody = SubroutineBody (VarDec [],[]);}, [])

        parseSubroutineDec [] [
            (Keyword "method"); (Keyword "int"); (IdentToken "fib");
            (Symbol '(');
            (Keyword "int"); (IdentToken "y");
            (Keyword "int"); (IdentToken "x");
            (Symbol ')');
            (Symbol '{'); 
            (Symbol '}');
            ]
            =? (Method
                 {SubroutineType = Type IntS;
                  Name = (SubroutineName (Identifier "fib"));
                  ParamList =
                   ParamList
                     [VarName (IntS,Identifier "y"); VarName (IntS,Identifier "x")];
                  SubroutineBody = SubroutineBody (VarDec [],[]);}, [])

        parseClassVarDec [
            (Keyword "static"); (Keyword "int"); (IdentToken "foo"); (Symbol ';')]
            =? (Static [VarName (IntS,Identifier "foo")], [])

        parseClassVarDec [
            (Keyword "static"); 
            (Keyword "int"); (IdentToken "foo"); (Symbol ','); 
            (Keyword "int"); (IdentToken "bar"); (Symbol ';');
            ]
            =? (Static [VarName (IntS,Identifier "bar"); VarName (IntS,Identifier "foo")], [])
    }
    Test "expression" {
        parseClassVarDec [
            (Keyword "static"); (Keyword "int"); (IdentToken "foo"); (Symbol ';')]
            =? (Static [VarName (IntS,Identifier "foo")], [])

        parseKeywordConstant [(Keyword "true"); ] =? (TrueC, [])
        parseKeywordConstant [(Keyword "false"); ] =? (FalseC, [])
        parseKeywordConstant [(Keyword "null"); ] =? (NullC, [])
        parseKeywordConstant [(Keyword "this"); ] =? (ThisC, [])
        
        parseOp [(Symbol '-'); ] =? Some (Minus, [])
        parseOp [(Symbol '+'); ] =? Some (Plus, [])

        parseTerm [(IntegerConstant 10us)] =? ((IntegerConstantT 10us), [])
        parseTerm [(Symbol '-'); (IntegerConstant 10us); (IntegerConstant 20us)] 
            =? (UnaryOpTerm (Negate, (IntegerConstantT 10us)), [IntegerConstant 20us])
        parseTerm [(IdentToken "foo")] =? ((Var "foo"), [])

        parseExpression [(IntegerConstant 3us); (Symbol '+'); (IntegerConstant 5us)]
            =? (ExpressionT (IntegerConstantT 3us,[OpTerm (Plus,IntegerConstantT 5us)]), [])

        parseExpression [(IntegerConstant 3us)] 
            =? (ExpressionT (IntegerConstantT 3us, []), [])
        //(i)
        //()
        parseExpressionList [] [(IntegerConstant 3us)] 
            =? (ExpressionList [ExpressionT (IntegerConstantT 3us,[])], [])
        parseExpressionList [] [(IdentToken "foo")] 
            =? (ExpressionList [ExpressionT (Var "foo",[])], [])
        parseExpressionList [] [(IdentToken "foo"); (IntegerConstant 4us)] 
            =? (ExpressionList [ExpressionT (Var "foo",[])], [IntegerConstant 4us])
        
        parseExpressionList [] [(Symbol ')')] 
            =? (ExpressionList [], [(Symbol ')')])

        let eCall = (SubroutineCallE (NakedCall (SubroutineName (Identifier "foo"), (ExpressionList []))))
        parseTerm [(IdentToken "foo"); (Symbol '('); (Symbol ')')] 
            =? (eCall, []) 
        
        let array = VarWithIndex (Identifier "foo", (ExpressionT ((Var "bar"), [])))
        parseTerm [(IdentToken "foo"); (Symbol '['); (IdentToken "bar"); (Symbol ']')]
            =? (array, [])
        
        }
    ()

let main args =
    runTests ()
    if status = 0 then
        stdout.WriteLine("OK, {0} tests passed.", count)
    status

//main()














