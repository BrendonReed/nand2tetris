[<AutoOpen>]
module CommandTypes

type Segment = 
    | Constant
    | LCL
    | ARG
    | THIS
    | THAT
    | Pointer
    | Temp
    | Static of string
type MemoryCommand = { Segment:Segment; Index:string }
type FunctionValues = { Name: string; NumArgs: int }
type Command =
    | Push of MemoryCommand
    | Pop of MemoryCommand
    | Add
    | Sub
    | Neg
    | Gt
    | Lt
    | Eq
    | And
    | Or
    | Not
    | Label of string
    | IfGoto of string
    | Goto of string
    | Function of FunctionValues
    | Call of FunctionValues
    | Return


