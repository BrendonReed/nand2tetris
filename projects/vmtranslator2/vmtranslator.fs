open System 
open System.IO
open System.Text.RegularExpressions
open Parser
open CodeWriter

[<EntryPoint>]
let main argv = 

    printfn "%A" argv
    let path = argv.[0]
    let pathInfo = FileInfo(path)
    let (+/) path1 path2 = Path.Combine(path1, path2)
    //might be a dir
    //might be a .vm file
    //if it's a dir, open all .vm files in the dir
    let files, outPath = 
        match pathInfo.Attributes = FileAttributes.Directory with
        | true -> 
            let d = DirectoryInfo(path)
            let vmFiles = d.EnumerateFiles("*.vm")
            let outFileName = d.Name + ".asm"
            (Seq.toList vmFiles, pathInfo.DirectoryName +/ outFileName)
        | false -> ([ pathInfo ], pathInfo.DirectoryName +/ pathInfo.Name.Replace(".vm", ".asm"))

    let oneFile (fileInfo:FileInfo) =
        let fileName = fileInfo.Name
        let path = fileInfo.FullName
        let parseH line = Parser.parse fileName line
        File.ReadLines(fileInfo.FullName) 
        |> Seq.toList
        |> List.map (fun i -> Regex.Replace(i, @"//.*", ""))
        |> List.map (fun i -> i.Trim())
        |> List.filter (fun i -> not (i.StartsWith("//")))
        |> List.filter (fun i -> not (String.IsNullOrWhiteSpace(i)))
        |> List.map (fun i -> i.Trim())
        |> List.map parseH
        |> List.map CodeWriter.translate
        |> List.fold (+) ""

    let lines =
        files
        |> List.map oneFile
        |> List.fold (+) ""

    let withPlumbing = 
        @"
        @256
        D=A
        @SP
        M=D
        @256
        D=A
        @LCL
        M=D
        @256
        D=A
        @ARG
        M=D
        @3000
        D=A
        @THIS
        M=D
        @4000
        D=A
        @THAT
        M=D
        " + (CodeWriter.translate (Parser.parse "" "call Sys.init 0")) + @"
        " + lines + @"
        "

    //printfn "%A" withPlumbing

    File.WriteAllText(outPath, withPlumbing)

    0
    //0-15 - virtual registers
    //16-255 - static variables
    //256-2047 - stack
    //2048-16483 - heap
    //16384-24575 - memory mapped io
    //
    //Hack registers 0-15 = R0-R15
    //registers 0-4 = 
    //SP - stack pointer, 
    //LCL - base of current functions local segment
    //ARG - base of current function's argument segment
    //THIS - base of current this segment in the heap
    //THAT - base of current that segment in heap
    //5-12 - temp
    //13-15  ??
    //16+ - static ie global
