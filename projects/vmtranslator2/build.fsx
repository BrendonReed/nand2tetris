// include Fake lib
#r @"packages/FAKE/tools/FakeLib.dll"
open Fake.FscHelper
open Fake

let buildDir = "./bin/"

Target "Clean" (fun _ ->
    CleanDir buildDir
)

Target "vmtranslator2.exe" (fun _ -> 
    [ "CommandTypes.fs"; "Parser.fs"; "CodeWriter.fs"; "vmtranslator.fs" ]
    |> Compile [Out (buildDir + "vmtranslator2.exe")]
    )

// Default target
Target "Default" (fun _ ->
    trace "Hello World from FAKE"
)

// start build
RunTargetOrDefault "vmtranslator2.exe"
