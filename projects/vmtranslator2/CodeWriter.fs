module CodeWriter
open CommandTypes

let mutable nextValue = 0
let uniqueSymbol () =
    nextValue <- nextValue + 1
    "symbol" + nextValue.ToString()

let popToD = 
    @"
    @SP
    M=M-1
    A=M
    D=M"

let popToDThenA = 
    popToD + @"
    @SP
    M=M-1
    A=M
    "
let popToA =
    @"
    @SP
    M=M-1
    A=M
    "
let pushD =
    @"
    @SP
    A=M
    M=D
    @SP
    M=M+1
    "

let popToSegment segment index =
    let popH segmentS index = 
        @"
        @" + index + "
        D=A
        @" + segmentS + @"
        D=M+D
        @R13
        M=D " + popToD + @"
        @R13
        A=M
        M=D
        "
    match segment with 
    | Constant -> ""
    | LCL -> popH "LCL" index
    | ARG -> popH "ARG" index
    | THIS -> popH "THIS" index
    | THAT -> popH "THAT" index
    | Pointer -> 
        @"
        @" + index + @"
        D=A
        @3
        D=A+D
        @R13
        M=D " + popToD + @"
        @R13
        A=M
        M=D
        "

    | Temp -> 
        @"
        @" + index + @"
        D=A
        @5
        D=A+D
        @R13
        M=D " + popToD + @"
        @R13
        A=M
        M=D
        "
    | Static fileName -> 
        popToD + @"
        @" + fileName + "." + index + @"
        M=D"

let pushFromSegment segment index =
    let pushH segmentS index = 
        @" //push " + segmentS.ToString() + @"
        @" + index + "
        D=A
        @" + segmentS + @"
        A=M+D
        D=M
        " + pushD
    match segment with 
    | Constant -> 
        @"
        @" + index + @"
        D=A
        " + pushD
    | LCL -> pushH "LCL" index
    | ARG -> pushH "ARG" index
    | THIS -> pushH "THIS" index
    | THAT -> pushH "THAT" index
    | Pointer -> 
        @"
        @" + index + "
        D=A
        @3
        A=A+D
        D=M
        "  + pushD

    | Temp -> 
        @"
        @" + index + "
        D=A
        @5
        A=A+D
        D=M
        "  + pushD
    | Static fileName -> 
        @"
        @" + fileName + "." + index + @"
        D=M" + pushD

let compare v =
    let trueS = "true:" + uniqueSymbol()
    let falseS = "false:" + uniqueSymbol()
    popToDThenA + @" //compare
        D=M-D
        @" + trueS + "
        D;" + v + "
        @0
        D=A
        @" + falseS + "
        0;JMP
    (" + trueS + ")
        @0
        D=A-1
    (" + falseS + ")
    "
    + pushD

let translate parsedLine =
    match parsedLine with
    | Push v -> 
        let { Segment = segment; Index = value } = v
        pushFromSegment segment value
    | Pop v -> 
        let { Segment = segment; Index = index } = v
        popToSegment segment index
    | Sub ->
        popToDThenA + @"
        D=M-D
        "
        + pushD
    | Add ->
        popToDThenA + @"
        D=D+M //Add
        "
        + pushD
    | Neg ->
        popToA + @"
        D=!M //Neg
        D=D+1
        "
        + pushD //!M 22 gives -23, so add 1?
    | Eq -> compare "JEQ" //true is -1, false is 0
    | Gt -> compare "JGT"
    | Lt -> compare "JLT"
    | And -> 
        popToDThenA + @" //And
        D=D&M
        "
        + pushD
    | Or ->
        popToDThenA + @" //Or
        D=D|M
        "
        + pushD
    | Not ->
        popToA + @" //Not
        D=!M
        "
        + pushD 
    | Label label -> 
        @" //label
        (" + label + ")
        "
    | IfGoto label -> 
        popToD + @" //IfGoto
        @" + label + @"
        D;JNE
        "
    | Goto label -> 
        @" //Goto
        @" + label + @"
        0;JMP
        "

    | Function v -> 
        let { Name = name; NumArgs = numArgs } = v
        let locals = 
            [|1..numArgs|] 
            |> Array.fold (
                fun acc i -> 
                    acc + 
                    @"
                    @0
                    D=A
                    " 
                    + pushD) 
                ""
        @" //Function " + name + @"
        (" + name + ")" + @"
        " + locals
    | Call v ->
        let { Name = name; NumArgs = numArgs } = v
        let returnAddress = "returnAfter" + name + ":" + uniqueSymbol()
        @" //Call " + name + @"
        @" + returnAddress + @"
        D=A
        " + pushD + @"
        @LCL
        D=M
        " + pushD + @"
        @ARG
        D=M
        " + pushD + @"
        @THIS
        D=M
        " + pushD + @"
        @THAT
        D=M
        " + pushD + @"
        @SP
        D=M
        @5
        D=D-A
        @" + numArgs.ToString() + @"
        D=D-A
        @ARG
        M=D
        @SP
        D=M
        @LCL
        M=D
        @" + name + @"
        0;JMP
        (" + returnAddress + @")
        "

    | Return -> 
        @" //Return " + @"
        @LCL
        D=M
        @R13 //Frame=LCL
        M=D
        @5
        D=D-A
        A=D
        D=M
        @R14 
        M=D //Ret = LCL - 5

        " + popToD + @"
        @ARG
        A=M
        M=D
        D=A //ARG
        @SP
        M=D+1

        @R13
        A=M-1
        D=M
        @THAT //=FRAME-1
        M=D

        @R13
        D=M
        @2
        D=D-A
        A=D
        D=M
        @THIS //=FRAME-2
        M=D

        @R13
        D=M
        @3
        D=D-A
        A=D
        D=M
        @ARG
        M=D

        @R13
        D=M
        @4
        D=D-A
        A=D
        D=M
        @LCL
        M=D

        @R14
        A=M
        0;JMP
        "


