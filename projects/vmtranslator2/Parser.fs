module Parser
open System
open System.Text.RegularExpressions
open CommandTypes

let reMatch value pattern = Regex.Match(value, pattern)
let namePattern = @"[\w.]+"
let labelPattern = @"[\w0-9_.:]+"

let mapSegmentH fileName s = 
    match s with 
    | "constant" -> Constant
    | "local" -> LCL
    | "argument" -> ARG
    | "this" -> THIS
    | "that" -> THAT
    | "pointer" -> Pointer
    | "temp" -> Temp
    | "static" -> Static fileName
    | _ -> failwith "invalid memory segment"

let parse fileName line = 
    let m pattern = reMatch line pattern
    let mapSegment = mapSegmentH fileName
    if (m @"push (\w+) (\d+)").Success 
    then 
        let r = m @"push (\w+) (\d+)" 
        Push { Segment = mapSegment r.Groups.[1].Value; Index = r.Groups.[2].Value }
    elif (m @"pop (\w+) (\d+)").Success 
    then 
        let r = m @"pop (\w+) (\d+)" 
        Pop { Segment = mapSegment r.Groups.[1].Value; Index = r.Groups.[2].Value }

    elif (m @"^add$").Success then Add
    elif (m @"sub$").Success then Sub
    elif (m @"^neg$").Success then Neg
    elif (m @"^gt$").Success then Gt
    elif (m @"^lt$").Success then Lt
    elif (m @"^eq$").Success then Eq
    elif (m @"^and$").Success then And
    elif (m @"^or$").Success then Or
    elif (m @"^not$").Success then Not
    elif (m (@"^label (" + labelPattern + @")")).Success 
    then 
        let r = m (@"^label (" + labelPattern + @")")
        Label r.Groups.[1].Value
    elif (m (@"^if-goto (" + labelPattern + @")$")).Success 
    then 
        let r = m (@"^if-goto (" + labelPattern + @")$")
        IfGoto r.Groups.[1].Value
    elif (m (@"^goto (" + labelPattern + @")$")).Success
    then 
        let r = m (@"^goto (" + labelPattern + @")$")
        Goto r.Groups.[1].Value
    elif (m (@"^function (" + namePattern + ") (\d+)$")).Success 
    then 
        let r = m (@"^function (" + namePattern + ") (\d+)$")
        Function { 
            Name = r.Groups.[1].Value; 
            NumArgs = Int32.Parse(r.Groups.[2].Value) }
    elif (m (@"^call (" + namePattern + ") (\d+)$")).Success 
    then 
        let r = m (@"^call (" + namePattern + ") (\d+)$")
        Call { Name = r.Groups.[1].Value; NumArgs = Int32.Parse(r.Groups.[2].Value) }
    elif (m @"^return$").Success then Return
    else failwith ("invalid command: " + line)
